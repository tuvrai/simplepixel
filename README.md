# simplepixel

The small library for easier pixel color manipulation in canvas

==Features==
Note: functions are related to x|y position of pixel on entire canvas, not array index of data
getValues - it returns object containing pixel color values (red,green,blue and alpha) in default values (0-255)
	Syntax:  simplePx.getValues(data,x,y);
	returns: obj {red,green,blue,alpha}
	
getConvertedValues - returns object containing pixel color values in rgb / hex
	Syntax: simplePx.getConvertedValues(data,x,y,type) - type: 'rgb' or 'hex'. Undefined will return same result as getValues(). 
		Note: with 'rgb' type - obj.alpha value is beetween 0 and 1; 'hex' - it's 0-255 value translated to hexadecimal
		returns: obj {red,green,blue,alpha}
		
getColor - returns color of object in ordinary string, compatible with css
	Syntax: simplePx.getColor(data,x,y,type,alpha) - type: 'rgb' (default) or 'hex'; alpha: false (default) / true - if true it will return color in rgba or in 8 digit hex, where the last 2 digits are alpha.

setColor - changes color of chosen pixel
	Syntax: simplePx.setColor(data,x,y,value) 
		- value: it can be an object with 4 properties (	red,green,blue,alpha - for example the one generated with getValues() / getConvertedValues()	) or simply rgb/rgba/hex value
		Note: the value should be correct (with either 4 properties or correct string value of color with no white characters: examples:
			rgb(15,34,56) | rgba(15,32,66,0.55) | #ff2133
		Note: it doesn't operate on hexalpha values
		
		
==Examples==
let color = simplePx.getColor(myCanvasData,15,35,'hex',false) - it will return color of pixel at 15|35 spot in hex (#??????) value.
simplePx.setColor(myCanvasData,15,35,'rgb(55,12,33)') - it will change the pixel color at 15|35 spot to rgb(55,12,33)

==Credits==
Author: Tuvrai

==License==
MIT




